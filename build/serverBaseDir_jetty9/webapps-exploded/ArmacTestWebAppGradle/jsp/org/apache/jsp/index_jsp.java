package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import DTO.TransitPathWeighted;
import DTO.TransitPathWeightedList;
import Messages.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta charset=\"UTF-8\">\r\n");
      out.write("\r\n");
      out.write("        <title>Armac Systems Technical Test - Lech Jankowski</title>\r\n");
      out.write("\r\n");
      out.write("        <!-- Bootstrap Core CSS -->\r\n");
      out.write("        <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">   \r\n");
      out.write("        \r\n");
      out.write("        <script>\r\n");
      out.write("            var geocoder;\r\n");
      out.write("            var map;\r\n");
      out.write("            var cities = [];\r\n");
      out.write("            \r\n");
      out.write("            // initialising google map\r\n");
      out.write("            function initialize() {\r\n");
      out.write("              geocoder = new google.maps.Geocoder();\r\n");
      out.write("              var mapOptions = {\r\n");
      out.write("                zoom: 4,\r\n");
      out.write("                center: {lat:53.3439,lng:23.0622} // centre of Europe\r\n");
      out.write("              }\r\n");
      out.write("              map = new google.maps.Map(document.getElementById('simpleMap'), mapOptions);\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            // plotting marker on the map with given location name\r\n");
      out.write("            function codeAddressPlot(a) {\r\n");
      out.write("              var address = a;\r\n");
      out.write("              var location;\r\n");
      out.write("              geocoder.geocode( { 'address': address}, function(results, status) {\r\n");
      out.write("                if (status == 'OK') {\r\n");
      out.write("                  location = results[0].geometry.location;\r\n");
      out.write("                  //map.setCenter(location);\r\n");
      out.write("                  var marker = new google.maps.Marker({\r\n");
      out.write("                      map: map,\r\n");
      out.write("                      position: location,\r\n");
      out.write("                      title: address\r\n");
      out.write("                  });\r\n");
      out.write("                } else {\r\n");
      out.write("                  //alert('Geocode was not successful for the following reason: ' + status);\r\n");
      out.write("                }\r\n");
      out.write("              });\r\n");
      out.write("            }      \r\n");
      out.write("            \r\n");
      out.write("            // in test - retrieving geolocation of a named location\r\n");
      out.write("            function codeAddressLocation(a) {\r\n");
      out.write("              var address = a;\r\n");
      out.write("              var location;\r\n");
      out.write("              geocoder.geocode( { 'address': address}, function(results, status) {\r\n");
      out.write("                if (status == 'OK') {\r\n");
      out.write("                  location = results[0].geometry.location;\r\n");
      out.write("                  //alert(location);\r\n");
      out.write("                  return location;\r\n");
      out.write("                } else {\r\n");
      out.write("                  //alert('Geocode was not successful for the following reason: ' + status);\r\n");
      out.write("                }\r\n");
      out.write("              });\r\n");
      out.write("             \r\n");
      out.write("            }              \r\n");
      out.write("\r\n");
      out.write("            // plotting all markers from global array \"cities\"\r\n");
      out.write("            function plotMarkers() {\r\n");
      out.write("                  var arrayLength = cities.length;\r\n");
      out.write("                  for (var i = 0; i < arrayLength; i++) {\r\n");
      out.write("                      codeAddressPlot(cities[i]);\r\n");
      out.write("                  }              \r\n");
      out.write("            }\r\n");
      out.write("          \r\n");
      out.write("            // in test - plotting a polyline on the map\r\n");
      out.write("            function plotPolylines() {\r\n");
      out.write("                \r\n");
      out.write("                var flightPlanCoordinates = [\r\n");
      out.write("                  {lat: 37.772, lng: -122.214},\r\n");
      out.write("                  {lat: 21.291, lng: -157.821},\r\n");
      out.write("                  {lat: -18.142, lng: 178.431},\r\n");
      out.write("                  {lat: -27.467, lng: 153.027}\r\n");
      out.write("                ];\r\n");
      out.write("                var path = [];\r\n");
      out.write("                path.push(new google.maps.LatLng(codeAddressLocation('Gdansk')));\r\n");
      out.write("                path.push(new google.maps.LatLng(codeAddressLocation('Zakopane')));\r\n");
      out.write("\r\n");
      out.write("                path.push(new google.maps.LatLng(37.77,-122.21));\r\n");
      out.write("                path.push(new google.maps.LatLng(21.29,-157.82));\r\n");
      out.write("        \r\n");
      out.write("                alert(path);\r\n");
      out.write("                \r\n");
      out.write("                var flightPath = new google.maps.Polyline({\r\n");
      out.write("                  path: path,\r\n");
      out.write("                  geodesic: true,\r\n");
      out.write("                  strokeColor: '#FF0000',\r\n");
      out.write("                  strokeOpacity: 1.0,\r\n");
      out.write("                  strokeWeight: 2\r\n");
      out.write("                });\r\n");
      out.write("\r\n");
      out.write("                flightPath.setMap(map);          \r\n");
      out.write("            }\r\n");
      out.write("         \r\n");
      out.write("        </script>        \r\n");
      out.write("        \r\n");
      out.write("    </head>\r\n");
      out.write("    <body onload=\"initialize();plotMarkers();\"> <!-- map and markers are drawn at each page load -->\r\n");
      out.write("        \r\n");
      out.write("        ");
 TransitPathWeightedList tpwl = (TransitPathWeightedList) (request.getSession().getAttribute("transits")); 
        String city;
        if (tpwl != null) {
            for (int i=0; i < tpwl.getPaths().size(); i++) {
                TransitPathWeighted tpw = tpwl.getPaths().get(i);
                for (int j=0; j < tpw.getCities().size(); j++) {
                    city = tpw.getCities().get(j);
        
      out.write("        \r\n");
      out.write("                    <script>\r\n");
      out.write("                        cities.push('");
      out.print(city);
      out.write("');\r\n");
      out.write("                    </script>\r\n");
      out.write("        ");

                }
            }
        }
        
      out.write("         \r\n");
      out.write("        \r\n");
      out.write("        \r\n");
      out.write("        \r\n");
      out.write("        <div class=\"container\">\r\n");
      out.write("          <h1>Armac Systems Technical Test</h1>\r\n");
      out.write("          <p>by Lech Jankowski</p> \r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("            <!-- Search and Notes areas ------------------------------------------>\r\n");
      out.write("\r\n");
      out.write("            <div class=\"panel-group\">\r\n");
      out.write("                <div class=\"panel panel-success\">\r\n");
      out.write("\r\n");
      out.write("                    <div class=\"panel-heading\">Import Data</div>\r\n");
      out.write("\r\n");
      out.write("                    <div class=\"panel-body\">\r\n");
      out.write("\r\n");
      out.write("                        <div class=\"col-xs-6\">\r\n");
      out.write("                            <label for=\"import\">Enter filename of Excel document to load data to database:</label>\r\n");
      out.write("                            <h6>Note! Excel Sheet column structure accepted: source,destination,time</h6>\r\n");
      out.write("                            <form method=\"post\" action=\"ActionServlet\" > \r\n");
      out.write("                                <div class=\"input-group\">\r\n");
      out.write("                                  <input name=\"filename_text\" type=\"text\" size=\"40\" class=\"form-control\" id=\"import\" >\r\n");
      out.write("                                  <span class=\"input-group-btn\">\r\n");
      out.write("                                    <button class=\"btn btn-default\" type=\"submit\">Start Import</button>\r\n");
      out.write("                                  </span>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <input type=\"hidden\" name=\"action\" value=\"ImportData\" id=\"submit_action\"/>\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <div class=\"col-xs-6\">\r\n");
      out.write("                            <form method=\"post\" action=\"ActionServlet\" > \r\n");
      out.write("                                <button class=\"btn btn-default\" type=\"submit\">Erase Neo4j DB</button>\r\n");
      out.write("                                <input type=\"hidden\" name=\"action\" value=\"DeleteData\" id=\"submit_action\"/>\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </div>                        \r\n");
      out.write("                        <div class=\"col-xs-6\">\r\n");
      out.write("                            ");
 out.println(AppWarningMessageImport.getText()); 
      out.write("\r\n");
      out.write("                        </div>                    \r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("\r\n");
      out.write("                <div class=\"panel panel-info\">\r\n");
      out.write("\r\n");
      out.write("                    <div class=\"panel-heading\">Search</div>\r\n");
      out.write("                    <div class=\"panel-body\">\r\n");
      out.write("                        <div class=\"col-xs-3\">\r\n");
      out.write("                            <form method=\"post\" action=\"ActionServlet\" > \r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"src\">Source City:</label>\r\n");
      out.write("                                    <h6>City names are case sensitive</h6>\r\n");
      out.write("                                    <input name=\"search_source\" type=\"text\" size=\"20\" class=\"form-control\" id=\"src\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"dst\">Destination City:</label>\r\n");
      out.write("                                    <input name=\"search_destination\" type=\"text\" size=\"20\" class=\"form-control\" id=\"dst\">\r\n");
      out.write("                                </div>                              \r\n");
      out.write("                                <button class=\"btn btn-default\" type=\"submit\" onclick=\"plotMarkers();\">Find Shortest Path</button>\r\n");
      out.write("                                <input type=\"hidden\" name=\"action\" value=\"FindShortestPath\" id=\"submit_action\"/>\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </div>     \r\n");
      out.write("                        <div class=\"col-xs-3\">\r\n");
      out.write("                            <form method=\"post\" action=\"ActionServlet\" > \r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"src\">Source City:</label>\r\n");
      out.write("                                    <h6>City names are case sensitive</h6>\r\n");
      out.write("                                    <input name=\"search_source\" type=\"text\" size=\"20\" class=\"form-control\" id=\"src\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"dst\">Destination City:</label>\r\n");
      out.write("                                    <input name=\"search_destination\" type=\"text\" size=\"20\" class=\"form-control\" id=\"dst\">\r\n");
      out.write("                                </div>                              \r\n");
      out.write("                                <button class=\"btn btn-default\" type=\"submit\" onclick=\"plotMarkers();\">Find All Possible Paths</button>\r\n");
      out.write("                                <input type=\"hidden\" name=\"action\" value=\"FindAllPaths\" id=\"submit_action\"/>\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </div>  \r\n");
      out.write("                        <div class=\"col-xs-3\">\r\n");
      out.write("                            <form method=\"post\" action=\"ActionServlet\" > \r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"src\">Source City:</label>\r\n");
      out.write("                                    <h6>City names are case sensitive</h6>\r\n");
      out.write("                                    <input name=\"search_source\" type=\"text\" size=\"20\" class=\"form-control\" id=\"src\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <button class=\"btn btn-default\" type=\"submit\" onclick=\"plotMarkers();\">Find All Neighbours</button>\r\n");
      out.write("                                <input type=\"hidden\" name=\"action\" value=\"SearchAllNeighbours\" id=\"submit_action\"/>\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </div>                         \r\n");
      out.write("                        <div class=\"col-xs-3\">\r\n");
      out.write("                            <form method=\"post\" action=\"ActionServlet\" > \r\n");
      out.write("                                <button class=\"btn btn-default\" type=\"submit\" onclick=\"plotMarkers();\">Display All Connections</button>\r\n");
      out.write("                                <input type=\"hidden\" name=\"action\" value=\"RenderAllConnections\" id=\"submit_action\"/>\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </div>  \r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>        \r\n");
      out.write("\r\n");
      out.write("            <!-- Results areas ---------------------------------------------------->\r\n");
      out.write("\r\n");
      out.write("            <div class=\"panel panel-default\">\r\n");
      out.write("                <div class=\"panel-heading\">Results</div>\r\n");
      out.write("                <div class=\"panel-body\">\r\n");
      out.write("                    ");
 out.println(AppWarningMessageSearch.getText()); 
      out.write("\r\n");
      out.write("                    </br>\r\n");
      out.write("                    ");
 out.println(SearchResultMessage.getText()); 
      out.write("\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>        \r\n");
      out.write("\r\n");
      out.write("            <!-- Map area --------------------------------------------------------->\r\n");
      out.write("\r\n");
      out.write("            <!-- Google maps goes here -->\r\n");
      out.write("            <div class=\"panel panel-default\">\r\n");
      out.write("                <div class=\"panel-heading\">Tour Map</div>\r\n");
      out.write("                <div class=\"panel-body\">\r\n");
      out.write("                        <div id=\"simpleMap\" style=\"height:500px;\"></div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("            <!-- end google maps -->          \r\n");
      out.write("\r\n");
      out.write("            <!-- Bootstrap Core JavaScript -->\r\n");
      out.write("            <script src=\"js/bootstrap.min.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        </div>\r\n");
      out.write("\r\n");
      out.write("        <!-- Google Maps -->\r\n");
      out.write("        <script async defer src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCj20qCYRGPtOtNdrjAstzbt3N8-0TOLqg&callback=initMap\" type=\"text/javascript\"></script>     \r\n");
      out.write("        \r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
