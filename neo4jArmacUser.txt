user:		armactestproduction
password:	b.Asl1GrNsaqFf.2bF9u0lJIHOGaC6l



// return City nodes without a TRANSIT_TO relationships
MATCH (n:City) WHERE NOT (n)-[:TRANSIT_TO]->() RETURN n

// return City nodes without a TRANSIT_TO any direction relationships
MATCH (n:City) WHERE NOT (n)-[:TRANSIT_TO]-() RETURN n

// return relationships attributes for the path
MATCH p =(a)-->(b)-->(c) WHERE a.name = 'London' AND c.name = 'Zurich' RETURN relationships(p)

// return nodes for the path
MATCH p =(a)-->(b)-->(c) WHERE a.name = 'London' AND c.name = 'Zurich' RETURN nodes(p)


// finds all paths between 2 nodes
match p=(a:City {name:'London'})-[r*..]->(b:City {name:'Zurich'})
with p, relationships(p) as rcoll // just for readability, alias rcoll
return p, reduce(totalTime=0, x in rcoll| totalTime + x.time) as totalTime
order by totalTime

//finds all paths between 2 nodes and sort them ASC with limit 1 to result in shortest time
match p=(a:City {name:'London'})-[r*..]->(b:City {name:'Zurich'})
with p, relationships(p) as rcoll // just for readability, alias rcoll
return p, reduce(totalTime=0, x in rcoll| totalTime + x.time) as totalTime
order by totalTime limit 1

// returns a list of all individual pairs of relationships between nodes
MATCH p = (n) - [r*1..1] -> () RETURN nodes(p), r

// find all nodes connected in 1st level with given naode with "name: 'name'"
MATCH p = (n:City {name: 'Dublin'}) - [r*1..1] -> () RETURN EXTRACT(n IN nodes(p)| n.name) AS cities, EXTRACT(r IN relationships(p)| r.time) AS transittimes