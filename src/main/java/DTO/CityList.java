/*
 * Copyright (C) 2017 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package DTO;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Lech Jankowski
 */
public class CityList {
    
    private ArrayList<String> cities;
    
    public CityList()
    {
        this.cities = new ArrayList<String>();
    }

    public CityList(ArrayList<String> cities) {
        this.cities = cities;
    }

    public ArrayList<String> getCityList() {
        return cities;
    }

    public void setCityList(ArrayList<String> cities) {
        this.cities = cities;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.cities);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CityList other = (CityList) obj;
        if (!Objects.equals(this.cities, other.cities)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CityList{" + "cities=" + cities + '}';
    }
    
    public String toStringFormatted() {
        String s = "";
        for (int i=0; i < cities.size(); i++) {
            s = s + cities.get(i)+" ,";
        }
        return s;
    }
    
    public void addCity(String city)
    {
        this.cities.add(city);
    }
    
    public boolean isEmpty() {
        return this.cities.isEmpty();
    }
}